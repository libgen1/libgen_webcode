#!/bin/sh

# Variables

  # source file
  sf="./codebase-catalog.txt"

# Program

for i in $(grep -E "\.[[:alnum:]]*$" ${sf} | sed 's/\(.*\.\)\([[:alnum:]]*$\)/\2/' | sort | uniq)
do
	echo ${i}
	grep -E "\.${i}$" ${sf}
done
