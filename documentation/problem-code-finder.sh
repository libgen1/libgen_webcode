#!/bin/sh

# Variables

  # source file containing list of problem code
  sf=./problem-code-list.txt

# Program

for i in $(sed -n 1,\$p ${sf})
do
	echo ${i}
	grep -r "${i}" ../* | sed 's/.*\/documentation.*//'
done
