#!/bin/sh

# This tool is for generating a catalog of components for
# this codebase.

# Note that this uses FreeBSD find syntax.
find -E ../ ! -regex "\.\./documentation.*" -and ! -regex "\.\./\.git.*" > ./codebase-catalog.txt

