Overview of Libgen Webcode

Q0) What's the purpose of this overview?

A0) The purpose of this overview is to get an understanding
of the fundamental building-blocks of this codebase.

Q1) How do we go about building an overview?

A1) First, we need to gather information on what tools
(software) are used to build this codebase.

We'll first analyse filenames to build a catalog; this
should give us a rough idea of what we've got to work
with. We will exclude <repo>/documentation/* from our
catalog.

Idea: we'll build a catalog-generator function so that we
can easily generate a cataglog that can easily be browsed.

Q2) How are we going to put our codebase-catalog to use?

A2) We'll sort through the catalog and generate a new
document that will group files by their filename
extension. Every file in the catalog that has a particular
filename extension will be grouped together. Files without
a filename extension will all go in a "miscellaneous"
group.

Q3) How are we going to identify serious problem-areas of
the codebase?

A3) We'll search the codebase for code that are of a
problematic nature. Input will be taken from the rest of
the team about what such code are and we'll list that
collected data about problem-code in a file. A tool that
locates and records problem-code (in the codebase) will be
built, based on the list of problem-code.
