# Libgen Webcode
This is the webcode portion of the code used to setup Library Genesis.

# Guide to code

Due to the complexity of the code, the fact it relies on out of date software and that many of the codes comments are in Russian this guide to the code is being written. This guide will (when complete) go through each php file and outline what it does.


## Software dependencies
The Libgen website currently runs on an outdated version of the LAMP stack. It uses php5 and cannot run on later releases of php due to its usage of the now deprecated `mysql_query()` function to connect to its mysql database. In addition, you must have the bcmath package installed for PHP.

The mysql database dumps seem to come from an earlier version of mysql so I have had problems with them containing the now deprecated `NO_AUTO_CREATE_USER` feature. This in my experience can be safely removed, a quick and hack way to do that is the command `sed -i 's/,NO_AUTO_CREATE_USER//' SQL_FILE.sql`. 

The final main requirement is apache2, which as far as I know has no complications.

An easy way to set all this up is a script located [here](https://gitlab.com/libgen1/libgen).

## Download locations
Compiled by @lucidhack.

### *Known Software Requirements:* 
  - PHP 5.4 or PHP 5.5 or PHP 5.6 
  - mysql extension 
  - mbstrings extension 
  - json (if using JSON API)

### http://gen.lib.rus.ec/code/libgen_legacy_catalog_20190831.rar
  - Activity: Last Updated OCT-2019
  - Key Folders Missing: 
    - /comics 
    - /standarts 
    - /magz 
  - Key Folders Present: 
    - /book (libgen) 
    - /foreignfiction 
    - /scimag 
  - Other Folders Present: 
    - /rss
  - Notes: Most up to date copy of /book, /foreignfiction, and /scimag

### https://gitlab.com/libgen1/libgen_webcode 
  - Activity: Currently Active as of 01-JAN-2020
  - Key Folders Missing: /comics /magz /standarts 
  - Key Folders Present: /book (libgen) /foreignfiction /scimag 
  - Other Folders Present: /img /rss /include
  - Notes: Lots of documentation on the database structure as well as the purpose/function of many files. Active repository. Incomplete file set.

### http://libgen.lc/libgen/code/libgen2016-05(nginx).rar 
- Activity: Last Update MAY-2016 Key 
- Folders Missing: /foreignfiction /scimag /comics /standarts /magz 
- Key Folders Present: /book (libgen) 
- Other Folders Present: /img /rss
- Notes:

### http://libgen.lc/comics/code/comics2016-05(nginx).rar 
- Activity: Last Update MAY-2016 Key Folders 
- Present: /comics 
- Other Folders Present: NONE
- Notes:

### http://libgen.lc/foreignfiction/code/foreignfiction2016-05(nginx).rar 
- Activity: Last Update MAY-2016 Key 
- Folders Present: /foreignfiction 
- Other Folders Present: NONE
- Notes:

### https://github.com/adulau/Library-Genesis 
- Activity: Last Update Oct 2016 
- Based on the following files: - libgen2012-11.rar - libgen2013-11.rar - libgen2014-06(nginx).rar - libgen2015-10(nginx).rar - libgen2016-05(nginx).rar 
- Key Folders Missing: /foreignfiction /comics /magz /standarts /scimag Key 
- Folders Present: /book (libgen) 
- Other Folders Present: /img /librarian /news /rss
- Notes: Has a lot of /librarian files

## Main site
These paths start in the main repo directory.

### batchsearchindex.php
Responds to a post request and searches in the database for answers, seems to be part of the search result system.

### config.sample.php
A sample config file. When used in production it is renamed `config.php`. The libgen setup script configures these up by default.

### connect.php
Connects to the database with the config information and displays and error if that fails.

### html.php
Sets out the html headers and footers to be used.

### index.php
The main page. It is the root of the website and the page you go to when searching for something. This file checks the users settings, displays html, searches the database for results and sets cookies. This is a very important file. There is an exect copy of it called `search.php`.

### json.php
This is the file that defines the json api, both serving the results and searching the database to get those results. More info on libgen's json api can be found [here](http://garbage.world/posts/libgen/) and [here](https://forum.mhut.org/viewtopic.php?f=17&t=6874).

### lang_en.php
Libgen supports two languages, Russian and English. It does this by defining a series of global variables for all the text strings shown to the user. These are then embedded into the HTML. The English ones are defined here. 

### lang_ru.php
Same as `lang_en.php` except Russian strings.

### mirrors.php
Creates an array of the libgen mirrors that will be shown to the user as options to download from. Also checks what row is being searched and customizes based on that.

### setlang.php
Checks what language the user wants and sets a cookie with that language choice. This makes the site display either Russian or English text depending on the users choice.

### book/bibtex.php
Outputs the information needed for a [bibtex](https://www.wikiwand.com/en/BibTeX) citation. BibTex is a citation manager for LaTex. The book is selected through a url parameter called `md5` which is equal to the books md5 hash value.

### book/index.php
Displays a chosen books information. Which book is ascertained through a url parameter called `md5` which is equal to the books md5 hash value.


## Foreign Fiction
These paths start in the `foreignfiction` directory.

### authors.php

### batchsearchindex.php

### config.sample.php
A sample config file. When used in production it is renamed `config.php`. The libgen setup script configures these up by default.

### index.php

### rss/index.php

### rss/rssclass.php

### rss/strings.php


## RSS
These paths start in the `rss` directory.

### index.php
Connects to the mysql database. It then creates a new rss feed using a class defined in `rssclass.php` and provides it to the user.

### rssclass.php
Creates a class and function that are used to create a custom RSS feed.


## Scimag
These paths start in the `scimag` directory.

### bibtex.php
Outputs the information needed for a [bibtex](https://www.wikiwand.com/en/BibTeX) citation. BibTex is a citation manager for LaTex. The article is selected through a url parameter called `doi` which is equal to the articles doi.

### config.sample.php
A sample config file. When used in production it is renamed `config.php`. The libgen setup script configures these up by default.

### index.php
The main page for searching the scimag database. Creates lists of results, and links to their individual pages and places to download them.

### journallinks.php
Seems to link results to journals so you can get info about them. Needs a closer look. Contains some helper functions.

### journals.php
Allows users to search by journal name. Provides a list alphabetically of all journals. 

### journaltable.php
Again stuff on journals. Seems to be involved in creating the tables used to list the results of searching by journal.