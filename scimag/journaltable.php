<?php 

require_once 'config.php';
require_once 'menu_'.$lang.'.html';
echo $libgensalogo;

if(!preg_match('|^[0-9]{1,5}$|', $_GET['journalid'])) {
    die("Wrong journal ID".$footer);
}




$journalid = $_GET['journalid'];


//заполняем шапку
$sqljournaltitle = "SELECT * FROM magazines where magazines.journalid = '".mysqli_real_escape_string($journalid)."'";
$resjournaltitle = mysqli_query($con, $sqljournaltitle);
$rowjournaltitle=mysqli_fetch_assoc($resjournaltitle);
$magazine = stripslashes($rowjournaltitle['Magazine']);
$issnp = stripslashes($rowjournaltitle['ISSNP']);
$issne = stripslashes($rowjournaltitle['ISSNE']);
$description = stripslashes($rowjournaltitle['Description']);
$publisher = stripslashes($rowjournaltitle['Publisher']);
$abbr = stripslashes($rowjournaltitle['Abbr']);
$category = stripslashes($rowjournaltitle['CATEGORY']);
$siteulrl = stripslashes($rowjournaltitle['Site_URL']);

if(preg_match('~^http~', $siteulrl)) {
    $siteulrl = "<tr><td width=200><b>URL:</b></td><td width=800><a href='".$siteulrl."'>URL</a></td></tr>";
} else {
    $siteulrl = '';
}

echo "<table width=1000 cellspacing=1 cellpadding=1 border = 1 align=center>
<tr><td width=200><b>".$LANG_MESS_58.":</b></td><td width=800>$magazine</td></tr>
<tr><td width=200><b>ISSN".$LANG_MESS_69.":</b></td><td width=800>$issnp</td></tr>
<tr><td width=200><b>ISSN".$LANG_MESS_70.":</b></td><td width=800>$issne</td></tr>
<tr><td width=200><b>".$LANG_MESS_72.":</b></td><td width=800>$description</td></tr>
<tr><td width=200><b>".$LANG_MESS_9.":</b></td><td width=800>$publisher</td></tr>
<tr><td width=200><b>".$LANG_MESS_13.":</b></td><td width=800>$category</td></tr>
<tr><td width=200><b>".$LANG_MESS_328.":</b></td><td width=800>$abbr</td></tr>

".$siteulrl."
<tr><td width=200><b>".$LANG_MESS_71.":</b></td><td width=800><a href='../scimag/journallinks.php?journalid=$journalid'>DOI</a></td></tr>
</table><br>";


//заполняем таблицу
$sqljournaltable = "select journalid, scimag.year,
GROUP_CONCAT(distinct(concat('v:', scimag.volume, ';i:', scimag.issue))
ORDER BY scimag.year,  scimag.volume +0, scimag.volume, scimag.issue +0, scimag.issue separator '|') as numbers
from scimag where
scimag.journalid = '".mysqli_real_escape_string($journalid)."'
GROUP BY scimag.journalid, scimag.year
order by scimag.journalid, scimag.year";
//echo $sqljournaltable;

//шапка для таблицы 
echo "<table width=1000 cellspacing=1 cellpadding=1 border = 1 align=center>
<thead><tr>
<td width=40><b>".$LANG_MESS_10."</b></td>
<td><b>".$LANG_MESS_56."</b></td>
</tr></thead>";

//обрабатываем строки
$resjournaltable = mysqli_query($con, $sqljournaltable);
while ($rowjournaltable=mysqli_fetch_assoc($resjournaltable)) {
                                                        $year = stripslashes($rowjournaltable['year']);
                                                        $issues = stripslashes($rowjournaltable['numbers']);



    $issue = explode('|', $issues);
    foreach($issue as $issuearr){

        $issuewithtagarr[] = str_replace('&v=&i=', '&v='.$year.'&i=', "<td><a href='index.php?s=&journalid=".$journalid."".str_replace(';', '', str_replace('v:', '&v=', str_replace('i:', '&i=',  $issuearr)))."'>".str_replace('vol.:;iss.:', 'iss.:', str_replace('v:', 'vol.:', str_replace('i:', 'iss.:',  $issuearr)))."</a></td>");

        //

    }
    unset($issuearr);
    $issuewithtag = implode('', $issuewithtagarr);


    $line = "<tr><td width=40>$year</td>$issuewithtag</tr>";
    unset($issuewithtagarr);

    echo $line;
}
echo "</table>";
echo $footer;
mysqli_free_result($resjournaltable);
mysqli_close($con);