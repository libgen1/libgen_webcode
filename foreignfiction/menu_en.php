<body>
	<ul id="menu">
		<li><a href="/setlang.php?lang=ru">RU</a><!-- Начало пункта-->
			
		</li><!-- Конец пункта-->
		<li><a href="https://forum.mhut.org/viewforum.php?f=22" class="drop">FORUM</a><!-- Начало пункта-->
			<div class="dropdown_1column">
				<div class="col_1">
					<ul>
						<a href="https://forum.mhut.org/viewforum.php?f=22">Forum</a> 

					</ul>
				</div>
			</div><!-- Конец контейнера-->
		</li><!-- Конец пункта Главная -->
		<li><a href="#" class="drop">DOWNLOAD</a><!-- Начало пункта-->
			<div class="dropdown_1column"><!-- Начало контейнера-->
				<div class="col_1">
					<ul>
						<li><a href="http://gen.lib.rus.ec/dbdumps/">DataBase Dump</a></li>
						<li><a href="http://libgen.io/foreignfiction/content/">CSV index</a></li>
						<li><a href="http://libgen.io/foreignfiction/code/">Source (PHP)</a></li>
						<li><a href="http://libgen.io/foreignfiction/import/">Import</a></li>
						<li><a href="http://gen.lib.rus.ec/foreignfiction/repository_torrent/">Torrents</a></li>
						<li><a href="http://fiction.libgen.pw">fiction.libgen.pw</a></li>
					</ul>
				</div>
			</div><!-- Конец контейнера-->
		</li><!-- Конец пункта -->
		<li><a href="/foreignfiction/librarian" class="drop">UPLOAD</a><!-- Начало пункта-->
			<div class="dropdown_1column"><!-- Начало контейнера-->
				<div class="col_1">
					<ul>
						<a href="http://libgen.io/foreignfiction/librarian"><h3>Web Uploader</h3></a>
						(Login:password look at the forum sitemap)
					</ul>   
				</div>
			</div><!-- Конец контейнера-->
		</li><!-- Конец пункта-->
		<li><a href="/foreignfiction/index.php?last=&f_lang=All&page=1" class="drop">LAST</a><!-- Начало пункта -->
			<div class="dropdown_2columns"><!-- Начало контейнера-->
				<div class="col_2">
					<ul>
						<a href="/foreignfiction/index.php?last=&f_lang=All&page=1">Last Added (100k)</a>

					</ul>
					<ul>
						<a href="http://libgen.io/fiction0day/">0-DAY (FTP, Not yet in BD)</a>

					</ul>  
					<ul>
						<a href="/foreignfiction/rss/index.php?page=1">RSS</a>

					</ul>
				</div>
			</div><!-- Конец контейнера-->
		</li><!-- Конец пункта Главная -->




		<li><a href="#" class="drop">AUTHORS</a><!-- Начало пункта-->
			<div class="dropdown_3lettercolumn"><!-- Начало контейнера-->
				<div class="col_0">
					<ul class="greybox">
						<li><a href="../foreignfiction/authors.php?letter=A">A</a></li>
						<li><a href="../foreignfiction/authors.php?letter=B">B</a></li>
						<li><a href="../foreignfiction/authors.php?letter=C">C</a></li>
						<li><a href="../foreignfiction/authors.php?letter=D">D</a></li>
						<li><a href="../foreignfiction/authors.php?letter=E">E</a></li>
						<li><a href="../foreignfiction/authors.php?letter=F">F</a></li>
						<li><a href="../foreignfiction/authors.php?letter=G">G</a></li>
						<li><a href="../foreignfiction/authors.php?letter=H">H</a></li>
						<li><a href="../foreignfiction/authors.php?letter=I">I</a></li>
					</ul>
				</div>
				<div class="col_0">
					<ul class="greybox">

						<li><a href="../foreignfiction/authors.php?letter=J">J</a></li>
						<li><a href="../foreignfiction/authors.php?letter=K">K</a></li>
						<li><a href="../foreignfiction/authors.php?letter=L">L</a></li>
						<li><a href="../foreignfiction/authors.php?letter=M">M</a></li>
						<li><a href="../foreignfiction/authors.php?letter=N">N</a></li>
						<li><a href="../foreignfiction/authors.php?letter=O">O</a></li>
						<li><a href="../foreignfiction/authors.php?letter=P">P</a></li>
						<li><a href="../foreignfiction/authors.php?letter=Q">Q</a></li>
						<li><a href="../foreignfiction/authors.php?letter=R">R</a></li>
					</ul>
				</div>
				<div class="col_0">
					<ul class="greybox">

						<li><a href="../foreignfiction/authors.php?letter=S">S</a></li>
						<li><a href="../foreignfiction/authors.php?letter=T">T</a></li>
						<li><a href="../foreignfiction/authors.php?letter=U">U</a></li>
						<li><a href="../foreignfiction/authors.php?letter=V">V</a></li>
						<li><a href="../foreignfiction/authors.php?letter=W">W</a></li>
						<li><a href="../foreignfiction/authors.php?letter=X">X</a></li>
						<li><a href="../foreignfiction/authors.php?letter=Y">Y</a></li>
						<li><a href="../foreignfiction/authors.php?letter=Z">Z</a></li>
					</ul>
				</div>
			</div><!-- Конец контейнера-->
		</li><!-- Конец пункта-->

		<li><a href="#" class="drop">OTHERS</a><!-- Начало пункта-->
			<div class="dropdown_2columns align_right"><!-- Начало контейнера -->
				<div class="col_2">
					<ul>
						<a href="/"><h3>Libgen</h3></a>
						<a href="/comics/"><h3>Comics</h3></a>
						<a href="http://magzdb.org/"><h3>Magazines</h3></a>
						<a href="/standarts/"><h3>Standarts</h3></a>
						<a href="http://libgen.io/pictures/"><h3>Paintings</h3></a>
					
						<a href="http://libgen.io/biblio/">Bibliography search (Ozon, Amazon, РГБ)</a>
						<a href="http://dc-poisk.no-ip.org/yndex.html">DC++ Fulltext Search (Yandex)</a>
						<a href="http://dc-poisk.no-ip.org/">P2P Fulltext Search (Sphinx, magnet-links)</a>



					</ul>   
				</div>
			</div><!-- Конец контейнера-->
		</li><!-- Конец пункта Главная -->
	</ul>
	<link rel='stylesheet' type='text/css' href='../paginator3000.css' />
	<script type='text/javascript' src='../paginator3000.js'></script>
	<style type='text/css'>
	.c { font-family: Georgia, 'Times New Roman', Times, serif; font-size: 11px; color: #000000; LETTER-SPACING: 0px; }
	A { text-decoration: none; }
	td { padding: 1px; }
	table { border-spacing: 1px 1px; }
	</style>
	<link rel="stylesheet" type="text/css" href="ns_tooltip.css" />
	<script type="text/javascript" src="ns_tooltip.js"></script>
