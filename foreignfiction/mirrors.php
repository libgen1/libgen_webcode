<?php

$mirrors = array(
    array(
        'title' => 'Gen.lib.rus.ec',
        'url' => 'http://93.174.95.29/fiction/{MD5_uc}'
    ),
    array(
        'title' => 'Libgen.lc',
        'url' => 'http://libgen.lc/foreignfiction/ads.php?md5={MD5_uc}'
    ),
    array(
        'title' => 'Z-Library',
        'url' => 'http://b-ok.cc/md5/{MD5_uc}'
    ),
    array(
        'title' => 'Libgen.me',
        'url' => 'http://fiction.libgen.me/item/detail/{MD5_lc}'
    ),
);