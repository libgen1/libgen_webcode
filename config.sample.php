<?php
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'bookuser');
define('DB_PASSWORD', 'MYSQL_BOOKUSERPASSWORD');
define('DB_NAME', 'bookwarrior');
$dbtable = 'updated';
$dbtable_edited = 'updated_edited';
$descrtable = 'description';
$descrtable_edited = 'description_edited';
$topictable = 'topics';

// full URL to libgen webcode root, no trailing slash
$url = '';

// problem resolution URL to mention in error messages
$errurl = '';

$maxlines = 25;

$mirror_0 = 'libgen.io';

// for RSS feeds
define('RSS_FEED_ITEMS', 100);
$servername = 'gen.lib.rus.ec';

// separator symbol
// TODO: replace with DIRECTORY_SEPARATOR
$filesep = '/';

// distributed repository
$repository = array(
    '0-300000' => '/shared_data/test_books',
);
$covers_repository = '/covers/';