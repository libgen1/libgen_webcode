<?php
require 'config.php';
require_once 'include/helper.php';

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

if (!$con) {
    error_log(mysqli_error($con));
    if (!$devmode) {
        http_response_code(500);
    }
    die(
        $htmlhead . "<font color='#A00000'><h1>Error</h1></font>Could not connect to the database: " .
            mysqli_error($con) .
            "<br>Cannot proceed.<p><a href='https://forum.mhut.org/viewtopic.php?t=6423'>Please, report on the error</a>." .
            $htmlfoot
    );
}
mysqli_query($con, "SET NAMES 'utf8'");